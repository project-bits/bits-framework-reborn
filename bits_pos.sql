-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 02 Nov 2018 pada 06.49
-- Versi server: 10.1.35-MariaDB
-- Versi PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bits_pos`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `settings`
--

CREATE TABLE `settings` (
  `id` int(25) NOT NULL,
  `param` varchar(255) NOT NULL,
  `value` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `settings`
--

INSERT INTO `settings` (`id`, `param`, `value`) VALUES
(1, 'name', 'Innovation Management'),
(2, 'address', 'Banten IT Solutions - Solusi Terbaik Kebutuhan Teknologi Informasi Anda'),
(3, 'kecamatan', NULL),
(4, 'city', NULL),
(5, 'zip', NULL),
(6, 'province', NULL),
(7, 'telp', NULL),
(8, 'note', NULL),
(9, 'path_windows', NULL),
(10, 'path_linux', NULL),
(11, 'print_purchasing', NULL),
(12, 'print_sales', NULL),
(13, 'receivables', NULL),
(14, 'barcode', NULL),
(15, 'design', NULL),
(16, 'stock', NULL),
(17, 'debt', NULL),
(18, 'multimarket', NULL),
(19, 'license', NULL),
(20, 'email', NULL),
(21, 'host', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `hp` varchar(20) NOT NULL,
  `password` text NOT NULL,
  `level` varchar(100) NOT NULL,
  `photo` varchar(250) NOT NULL,
  `nip` varchar(30) NOT NULL,
  `jabatan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `hp`, `password`, `level`, `photo`, `nip`, `jabatan`) VALUES
(1, 'System Administrator', 'admin', 'admin@bits.co.id', '0819678048', '$2y$10$.aV5pBuKOM/edW5qik1s5.UcEJcgRNF2FaFKP6ptzYMAAvnroiWru', 'admin', '34.jpg', '11212262', 'IT Project Manager'),
(2, 'Nurul Imam', 'nurulimam', 'me@nurulimam.com', '0819678048', '$2y$10$SfSjk56kV9SoEVmXSL9rIuiHpdoJxXoTjKZCQQjgzvc1Z4xiN.3ue', 'user', '34.jpg', '11212076', 'System Analyst'),
(3, 'Hadri Sadega', 'hadri', 'hadrifakot@gmail.com', '081911164279', '$2y$10$ELb3PwXSOZ.cR38IKc06keKEtvl5B0pBTFZbDMTQJx/6BIycBw3wK', 'user', '34.jpg', '11212298', 'IT Support');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
