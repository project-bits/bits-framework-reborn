<?php
/**
 * PHP version 5
 * Frontend Controller Goes here
 *
 * @category Controller
 * @package  BITS\BITS
 * @author   Nurul Imam <me@nurulimam.com>
 * @license  https://creativecommons.org/licenses/by-nc-nd/3.0/ Creative Commons
 * @link     https://bits.co.id
 */
use BITS\BITS;

/**
 * GET Method Request
 */
$this->respond(
    'GET', // Request Method
    '/?',  // Path ex. http://localhost/about/
    function ($request, $response, $service) {
        $service->title = 'About'; // Variable can use $this->title
        $service->render('app/Views/public/about.php'); // Path to view
    }
);
