<?php
use BITS\Auth;
use BITS\BITS;
use App\User;
use App\Settings;
use Tools\Date;
use Tools\API;

/**
 * Uncomment if use database connections.
 * Please go to app/config/config.php & edit your database connection.
 */
new BITS;

/*
 * Login Page Controller.
 */
$route->respond(
    '/?',
    function ($request, $response, $service) {
        $service->title = Settings::name();
        $service->render('app/Views/public/home.php');
    }
);

/*
 * Login Page Controller.
 */
$route->respond(
    '/login/?',
    function ($request, $response, $service) {
        if (DBNAME == "") {
            $_SESSION['alert'] = 'danger';
            $_SESSION['message'] = 'Please Configure & Install Database...!';
        }
        if (Auth::validUsers()) {
            Auth::redirect("/system/dashboard/");
        } else {
            $service->title = Settings::name()." - System Login";
            $service->render('app/Views/system/login.php');
        }
    }
);

/*
 * Check and Validate user login.
 * If user successfully logged in, redirect to dashboard.
 */
$route->respond(
    'POST',
    '/login/?',
    function ($request, $response, $service) {
        if (isset($_POST['submit'])) {
            Auth::login("users", $_POST['username'], $_POST['password']);
            if (isset($_SESSION['salt']) && isset($_SESSION['username'])) {
                Auth::redirect('/system/dashboard/');
            } else {
                Auth::redirect('/login/');
            }
        }
    }
);

/*
 * Logout Page Controller.
 * Destroy all session and redirect to login page.
 */
$route->respond(
    '/logout/?',
    function ($request, $response, $service) {
        Auth::logout();
        Auth::redirect('/login/');
    }
);

/*
 * Initialize Frontend Master Layout.
 */
$route->respond(
    function ($request, $response, $service, $app) use ($route) {
        /*
         * Handle Error Exception message to all Controllers.
         */
        $route->onError(
            function ($route, $err_msg) {
                $route->service()->flash($err_msg);
                $route->service()->back();
            }
        );

        /*
         * Register layouts master to all Controllers.
         */
        $service->layout('app/Views/public/index.php');
    }
);

/**
 * Frontend Controllers.
 */
foreach (['about'] as $controller) {
    $route->with("/$controller", "app/Controllers/public/$controller.php");
}


/*
 * Initialize Backend Master layout.
 * Initialize Controllers with some service and layouts.
 * Add controller to this block or separate use loop Controllers.
 */
$route->respond(
    function ($request, $response, $service, $app) use ($route) {
        /*
         * Handle Error Exception message to all Controllers.
         */
        $route->onError(
            function ($route, $err_msg) {
                $route->service()->flash($err_msg);
                $route->service()->back();
            }
        );

        /*
         * Register layouts master to all Controllers.
         */
        $service->layout('app/Views/system/layouts/index.php');
    }
);

/**
 * Backend Controllers.
 * Administrator privilages Controllers.
 */
foreach (['pengaturan'] as $controller) {
    /**
     * Check is admin or users. Protect page only administrator to
     * access this page / controller.
     */
    if (Auth::checkAdmin()) {
        $route->with("/system/$controller", "app/Controllers/system/$controller.php");
    }
}

/**
 * Backend Controllers.
 * Administrator and users privilages Controllers.
 */
foreach (['dashboard', 'users', 'report'] as $controller) {
    /**
     * Check is admin or users. Protect page only administrator & users to
     * access this page / controller.
     */
    if (Auth::validUsers()) {
        $route->with("/system/$controller", "app/Controllers/system/$controller.php");
    }
}

/*
 * Route all error request to error page.
 */
$route->onHttpError(
    function ($code, $router, $service) {
        if ($code >= 400 && $code < 500) {
            include 'themes/default/404.php';
        } elseif ($code >= 500 && $code <= 599) {
            include 'themes/default/404.php';
        }
    }
);

require_once 'app/Models/lib.php';
