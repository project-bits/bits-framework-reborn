<?php
/**
 * PHP version 5
 * Backend Controller - Dashboard
 *
 * @category Controller
 * @package  BITS\BITS
 * @author   Nurul Imam <me@nurulimam.com>
 * @license  https://creativecommons.org/licenses/by-nc-nd/3.0/ Creative Commons
 * @link     https://bits.co.id
 */
use BITS\BITS;
use App\Settings; // Config Global Settings

/**
 * GET Method Request
 */
$this->respond(
    'GET',
    '/?',
    function ($request, $response, $service) {
        $service->title = 'Dashboard - '.Settings::name(); // Use $this->title
        $service->render('app/Views/system/dashboard.php');  // Path to backend view
    }
);
