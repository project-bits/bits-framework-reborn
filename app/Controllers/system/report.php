<?php
/**
 * PHP version 5
 * Backend Controller - Report
 *
 * @category Controller
 * @package  BITS\BITS
 * @author   Nurul Imam <me@nurulimam.com>
 * @license  https://creativecommons.org/licenses/by-nc-nd/3.0/ Creative Commons
 * @link     https://bits.co.id
 */

// Display Form Report
$this->respond(
    'GET',
    '/?',
    function ($request, $response, $service) {
        $service->title = 'Report';
        $service->render('app/Views/system/form/form_report.php');
    }
);

// Create & Update
$this->respond(
    'POST',
    '/?',
    function ($request, $response, $service) {
        if (isset($_POST['pdf'])) {
            $service->title = 'Report';
            $service->layout('app/Views/system/layouts/report.php'); // Custom Layout
            $service->render('app/Views/system/report/pdf_report.php');
        } elseif (isset($_POST['excel'])) {
            $service->title = 'Report';
            $service->layout('app/Views/system/layouts/report.php'); // Custom Layout
            $service->render('app/Views/system/report/xls_report.php');
        }
    }
);
