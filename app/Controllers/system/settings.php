<?php
/**
 * PHP version 5
 * Backend Controller - Settings
 *
 * @category Controller
 * @package  BITS\BITS
 * @author   Nurul Imam <me@nurulimam.com>
 * @license  https://creativecommons.org/licenses/by-nc-nd/3.0/ Creative Commons
 * @link     https://bits.co.id
 */
use App\Settings;
use App\Backup;
use BITS\Download;
use BITS\Auth;

// View All
$this->respond(
    'GET',
    '/?',
    function ($request, $response, $service) {
        $service->title = 'Settings';
        $service->render('app/Views/system/settings.php');
    }
);

// Create & Update
$this->respond(
    'POST',
    '/?',
    function ($request, $response, $service) {
        if (isset($_POST['save'])) {
            Settings::update();
        }
        Auth::redirect("/system/settings/");
    }
);

// Backup
$this->respond(
    'GET',
    '/backup/?',
    function ($request, $response, $service) {
        $file = Backup::now();
        $folder = "backup/";

        $args = [
            'download_path'     =>  $folder,
            'file'              =>  $file,
            'extension_check'   =>  true,
            'referrer_check'    =>  false,
            'referrer'          =>  null,
        ];

        new Download($args);
        Download::get_download_hook();

        if (Download::$download_hook['download'] == true) {
            Download::get_download();
        }
    }
);
