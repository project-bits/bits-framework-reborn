<?php
/**
 * PHP version 5
 * Backend Controller - Users
 *
 * @category Controller
 * @package  BITS\BITS
 * @author   Nurul Imam <me@nurulimam.com>
 * @license  https://creativecommons.org/licenses/by-nc-nd/3.0/ Creative Commons
 * @link     https://bits.co.id
 */
use App\User;
use BITS\Auth;

// View All
$this->respond(
    'GET',
    '/?',
    function ($request, $response, $service) {
        $service->title = 'Users';
        if (Auth::checkAdmin() === true) {
            $service->users = User::all();
        } else {
            $service->users = User::find($_SESSION['id']);
        }
        $service->render('app/Views/system/users.php');
    }
);

// Create & Update
$this->respond(
    'POST',
    '/?',
    function ($request, $response, $service) {
        if (isset($_POST['create'])) {
            User::add();
        } elseif (isset($_POST['update'])) {
            User::update();
        }
        Auth::redirect("/system/users/");
    }
);

// Delete
$this->respond(
    'GET',
    '/delete/[:id]',
    function ($request, $response, $service) {
        User::delete($request->id);
        Auth::redirect("/system/users/");
    }
);
