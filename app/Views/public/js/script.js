
$(document).ready( function() {
    var ListFonts = [
        "'Pacifico'",
        "'Architects Daughter'",
        "'Shadows Into Light Two'",
        "'Tangerine'",
        "'Great Vibes'",
        "'Chewy'",
        "'Pinyon Script'",
        "'Dancing Script'"
    ];

    function loadFonts(i) {
        return ListFonts[i];
    }

    function randNumber() {
        return Math.floor(Math.random() * ListFonts.length);
    }

    setInterval(function(){
        $('.change').hide().css('font-family', loadFonts(randNumber())).fadeIn("slow");
    }, 3000);
});
