<?php
/**
* Header
*/
$this->partial('themes/default/header.php');

/**
* Menu
*/
$this->partial('themes/default/menu.php');

/**
* Page Content
*/
$this->yieldView();

/**
* Sidebar
*/
$this->partial('themes/default/sidebar.php');


/**
* Footer
*/
$this->partial('themes/default/footer.php');
