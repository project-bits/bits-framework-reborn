<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?php echo $this->title; ?></h2>
    </div>
    <div class="col-lg-2">
    <?php
    use BITS\Level;
    use App\User;

    if (Level::isAdmin()) { ?>
    <button  rel="tooltip" data-placement="left" title="Create User" style="margin-top: 20px;" class="btn btn-success btn-circle btn-lg pull-right" type="button" data-toggle="modal" data-target=".create"><i class="fa fa-plus"></i></button>
    <?php } ?>
    </div>
</div>

<div class="wrapper wrapper-content animated pulse">
    <div class="table-responsive">
        <table id="table"
        data-toggle="table"
        data-show-columns="true"
        data-search="true"
        data-show-toggle="true"
        data-show-pagination-switch="false"
        data-show-refresh="false"
        data-page-size="5"
        data-page-list="[5,10,25,50,100]"
        data-pagination-first-text="First"
        data-pagination-pre-text="Previous"
        data-pagination-next-text="Next"
        data-pagination-last-text="Last"
        data-pagination="true">
            <thead>
                <tr>
                    <th data-sortable="true">Nama</th>
                    <th data-sortable="true">NIP</th>
                    <th data-sortable="true">Jabatan</th>
                    <th data-sortable="true">Email</th>
                    <th data-sortable="true">No. Hp</th>
                    <th data-sortable="true">Level</th>
                    <th class="text-center" data-sortable="false">Action</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($this->users as $users) { ?>
                <tr>
                    <td><?php echo $users['name']; ?></td>
                    <td><?php echo $users['nip']; ?></td>
                    <td><?php echo $users['jabatan']; ?></td>
                    <td><?php echo $users['email']; ?></td>
                    <td><?php echo $users['hp']; ?></td>
                    <td><?php echo ucfirst($users['level']); ?></td>
                    <td align="center">
                        <a class="btn btn-sm btn-circle btn-info" rel="tooltip" data-placement="top" title="Edit" href="javascript:;"
                            data-id="<?php echo $users['id']; ?>"
                            data-name="<?php echo $users['name']; ?>"
                            data-nip="<?php echo $users['nip']; ?>"
                            data-jabatan="<?php echo $users['jabatan']; ?>"
                            data-email="<?php echo $users['email']; ?>"
                            data-hp="<?php echo $users['hp']; ?>"
                            data-username="<?php echo $users['username']; ?>"
                            data-level="<?php echo $users['level']; ?>"
                            data-toggle="modal"
                            data-target=".update">
                            <i class="fa fa-pencil"></i>
                        </a>
                        <?php if (Level::isAdmin()) { ?>
                        <a class="btn btn-sm btn-circle btn-danger" rel="tooltip" data-placement="top" title="Remove" href="javascript:;"
                            data-id="<?php echo $users['id']; ?>"
                            data-url="system/users"
                            data-toggle="modal"
                            data-target=".delete">
                            <i class="fa fa-trash"></i>
                        </a>
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<?php $this->partial('app/Views/system/users/create.php'); ?>
<?php $this->partial('app/Views/system/users/update.php'); ?>
<?php $this->partial('app/Views/system/modals/delete.php'); ?>
