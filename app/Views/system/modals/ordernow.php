<div class="modal inmodal ordernow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content animated fadeInDown">
            <form autocomplete="off" method="POST" action="" enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-car modal-icon"></i>
                    <h5 class="modal-title">Order Driver</h5>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Waktu Penjemputan</label>
                        <div class='input-group date' id='datetimepicker'>
                            <input type='text' name="waktujemput" class="form-control" required/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label>Lokasi Penjemputan</label>
                        <input name="lokasijemput" id="lokasijemput" type="text"
                                class="form-control"
                                placeholder="Lokasi Penjemputan"
                                maxlength="200" required />
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label>Jumlah Penumpang</label>
                        <input name="penumpang" id="penumpang" type="number"
                                class="form-control"
                                placeholder="Jumlah Penumpang"
                                maxlength="200" required />
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label>Lokasi Tujuan</label>
                        <input name="tujuan" id="tujuan" type="text"
                                class="form-control"
                                placeholder="Lokasi Tujuan"
                                maxlength="200" required />
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label>Event</label>
                        <input name="event" id="event" type="text"
                                class="form-control"
                                placeholder="Event"
                                maxlength="200" required />
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label>No. Hp Yang Dihubungi</label>
                        <input name="hphubungi" id="hphubungi" type="text"
                                class="form-control"
                                placeholder="No. Hp Yang Dihubungi"
                                maxlength="20" />
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label>No. Hp 1 (Opsional)</label>
                        <input name="hp1" id="hp1" type="text"
                                class="form-control"
                                placeholder="No. Hp Yang Dihubungi"
                                maxlength="20" />
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label>No. Hp 2 (Opsional)</label>
                        <input name="hp2" id="hp2" type="text"
                                class="form-control"
                                placeholder="No. Hp Yang Dihubungi"
                                maxlength="20" />
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label>No. Hp 3 (Opsional)</label>
                        <input name="hp3" id="hp3" type="text"
                                class="form-control"
                                placeholder="No. Hp Yang Dihubungi"
                                maxlength="20" />
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label>No. Hp 4 (Opsional)</label>
                        <input name="hp4" id="hp4" type="text"
                                class="form-control"
                                placeholder="No. Hp Yang Dihubungi"
                                maxlength="20" />
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label>Keterangan</label>
                        <input name="keterangan" id="keterangan" type="text"
                                class="form-control"
                                placeholder="Nama Pegawai"
                                maxlength="200" />
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-sm btn-submit" name="ordernow">
                        <i class="fa fa-save"></i> Save
                    </button>
                    <button class="btn btn-danger btn-sm" data-dismiss="modal">
                        <i class="fa fa-close"></i> Close
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>