<div class="modal animated swing done" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Done ?</h4>
            </div>
            <div class="modal-body">
                Are you sure this process ?
            </div>
            <div class="modal-footer">
                <a href="javascript:;" class="btn btn-danger" id="done">Yes</a>
                <button type="button" class="btn btn-info" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>
