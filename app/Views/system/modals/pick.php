<div class="modal inmodal pick" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content animated fadeInDown">
            <form method="POST" action="" enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-server modal-icon"></i>
                    <h5 class="modal-title">Pick Mobil & Driver</h5>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Pilih Driver</label>
                            <select name="id_driver" id="driver" class="form-control select2" style="width:100%" required>
                                <option></option>
                                <?php use App\Driver; foreach (Driver::all() as $driver) { ?>
                                    <option value="<?php echo $driver['id']; ?>"><?php echo $driver['nama']; ?></option>
                                <?php } ?>
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <label>Pilih Mobil</label>
                            <input type="hidden" name="id" id="id" />
                            <select name="id_mobil" id="mobil" class="form-control select2" style="width:100%" required>
                                <option></option>
                                <?php use App\Mobil; foreach (Mobil::all() as $mobil) { ?>
                                    <option value="<?php echo $mobil['id']; ?>"><?php echo $mobil['nama']; ?> - <?php echo $mobil['nopol']; ?></option>
                                <?php } ?>
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success btn-sm btn-submit" name="pick">
                        <i class="fa fa-save"></i> Save
                    </button>
                    <button class="btn btn-danger btn-sm" data-dismiss="modal">
                        <i class="fa fa-close"></i> Close
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
