<div class="modal animated swing reject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Reject Order ?</h4>
            </div>
            <div class="modal-body">
                Are you sure reject this order ?
            </div>
            <div class="modal-footer">
                <a href="javascript:;" class="btn btn-danger" id="reject">Yes</a>
                <button type="button" class="btn btn-info" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>
