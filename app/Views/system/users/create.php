<div class="modal inmodal fullscreen create" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content animated fadeInDown">
            <form autocomplete="off" method="POST" action="" enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-users modal-icon"></i>
                    <h5 class="modal-title">Create User</h5>
                </div>
                <div class="modal-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Full Name</label>
                            <input name="name" id="name" type="text"
                                    class="form-control"
                                    placeholder="Full Name"
                                    maxlength="50" required />
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <label>NIP</label>
                            <input name="nip" id="nip" type="text"
                                    class="form-control"
                                    placeholder="NIP"
                                    maxlength="50" required />
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <label>Username</label>
                            <input name="username" id="username" type="text"
                                    class="form-control"
                                    placeholder="Username"
                                    maxlength="50" required />
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input name="password" id="password" type="password"
                                class="form-control"
                                placeholder="Leave blank if do not to change"
                                data-minlength="6"
                                data-error="Please fill min 6 word." />
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Email</label>
                            <input name="email" id="email" type="email"
                                    class="form-control"
                                    placeholder="Email"
                                    maxlength="50" required />
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <label>Jabatan</label>
                            <input name="jabatan" id="jabatan" type="text"
                                    class="form-control"
                                    placeholder="Jabatan"
                                    maxlength="50" required />
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <label>No. Hp</label>
                            <input name="hp" id="hp" type="text"
                                    class="form-control"
                                    placeholder="No. Hp"
                                    maxlength="50" required />
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <label>Bagian</label>
                            <select name="level" id="level2" class="form-control select2" style="width:100%" required>
                                    <option value="admin">Administrator</option>
                                    <option value="user">User</option>
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Upload Photo</label>
                            <input name="photo" id="photo" type="file"
                                    class="form-control"
                                    placeholder="Upload Photo"
                                    required />
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success btn-sm btn-submit" name="create">
                        <i class="fa fa-save"></i> Save
                    </button>
                    <button class="btn btn-danger btn-sm" data-dismiss="modal">
                        <i class="fa fa-close"></i> Close
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
