<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=hasil.xls");
?>
<?php
use BITS\BITS;
use Tools\Date;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo $this->title; ?></title>
        <link rel="stylesheet" href="/assets/bootstrap/dist/css/bootstrap.min.css">
    </head>
    <body onload="window.print()">
        <div class="col-lg-12">
            <h4 class="text-center"><?php echo strtoupper($this->title); ?></h4>
            <div class="col-xs-4">
                <table class="table" style="font-size: 10px">
                <tr>
                    <td style="border:0">Priode</td>
                    <td style="border:0"> : </td>
                    <td style="border:0"><?php echo $this->priode; ?></td>
                </tr>
                </table>
            </div>
        </div>
        <br>
        <div class="col-lg-12">
            <table class="table table-bordered" style="margin-top:25px;font-size: 10px">
                <thead>
                    <tr>
                        <th style="vertical-align: middle">ID</th>
                        <th style="vertical-align: middle">Nama Pemesan</th>
                        <th style="vertical-align: middle">No. Hp Pemesan</th>
                        <th style="vertical-align: middle">Waktu Penjemputan</th>
                        <th style="vertical-align: middle">Lokasi Penjemputan</th>
                        <th style="vertical-align: middle">Tujuan</th>
                        <th style="vertical-align: middle">Event</th>
                        <th style="vertical-align: middle">Keterangan</th>
                        <th style="vertical-align: middle">Nama Driver</th>
                        <th style="vertical-align: middle">No. Hp Driver</th>
                        <th style="vertical-align: middle">Nama Mobil</th>
                        <th style="vertical-align: middle">No. Polisi</th>
                        <th style="vertical-align: middle">Waktu Selesai</th>
                        <th style="vertical-align: middle">Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($this->report as $report) { ?>
                    <tr>
                        <td><?php echo $report['id']; ?></td>
                        <td><?php echo $report['namauser']; ?></td>
                        <td><?php echo $report['hpuser']; ?></td>
                        <td><?php echo Date::convert($report['waktujemput'], "time")." - ".Date::convert($report['waktujemput'], "slash"); ?></td>
                        <td><?php echo $report['lokasijemput']; ?></td>
                        <td><?php echo $report['tujuan']; ?></td>
                        <td><?php echo $report['event']; ?></td>
                        <td><?php echo $report['keterangan']; ?></td>
                        <td><?php echo $report['namadriver']; ?></td>
                        <td><?php echo $report['hpdriver']; ?></td>
                        <td><?php echo $report['namamobil']; ?></td>
                        <td><?php echo $report['nopol']; ?></td>
                        <td><?php echo Date::convert($report['tiba'], "time")." - ".Date::convert($report['tiba'], "slash"); ?></td>
                        <td><?php echo $report['status']; ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </body>
</html>
