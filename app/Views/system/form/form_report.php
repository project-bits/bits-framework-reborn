<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?php echo $this->title; ?></h2>
    </div>
    <div class="col-lg-2">
    </div>
</div>

<div class="wrapper wrapper-content">
    <form action="/system/report/" method="POST">
        <div class='col-md-3'>
            <div class="form-group">
                <div class='input-group date' id='datetimepicker'>
                    <input type='text' name="start" class="form-control" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
        <div class='col-md-3'>
            <div class="form-group">
                <div class='input-group date' id='datetimepicker1'>
                    <input type='text' name="end" class="form-control" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-4">
            <div class="form-group">
                <button class="btn btn-danger" name="pdf"><i class="fa fa-file-pdf-o"></i> &nbsp;Export PDF</button>
                <button class="btn btn-primary" name="excel"><i class="fa fa-file-excel-o"></i> &nbsp;Export Excel</button>
            </div>
        </div>
    </form>
</div>

