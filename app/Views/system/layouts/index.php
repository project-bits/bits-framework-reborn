<?php
/**
* Header
*/
$this->partial('app/Views/system/layouts/header.php');

/**
 * Sidebar
 */
$this->partial('app/Views/system/layouts/sidebar.php');

/**
* Page Content
*/
$this->yieldView();

/**
* Footer
*/
$this->partial('app/Views/system/layouts/footer.php');
