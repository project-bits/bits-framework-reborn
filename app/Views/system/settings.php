<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?php echo $this->title; ?></h2>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<?php
use App\Pengaturan;
?>
<div class="wrapper wrapper-content">
    <form action="" method="POST">
        <div class="col-md-6">
            <div class="form-group">
                <label>Nama Perusahaan</label>
                <input name="nama" id="nama" type="text" class="form-control" value="<?php echo Pengaturan::nama(); ?>" maxlength="100" required />
            </div>

            <div class="form-group">
                <label>Text Ads</label>
                <textarea name="alamat" id="alamat" class="form-control" required><?php echo Pengaturan::alamat(); ?></textarea>
            </div>

            <!--div class="form-group">
                <label>Desain Minimalis</label>
                <select name="desain" id="desain" class="form-control select2" style="width:100%" required>
                    <option value="y" <?php echo (Pengaturan::desain() == "y") ? "selected" : ""; ?>>Iya</option>
                    <option value="t" <?php echo (Pengaturan::desain() == "t") ? "selected" : ""; ?>>Tidak</option>
                </select>
            </div-->
            
            <!--div class="form-group">
                <label>Kecamatan</label>
                <input name="kecamatan" id="kecamatan" type="text" class="form-control" value="<?php echo Pengaturan::kecamatan(); ?>" maxlength="32" required />
            </div>

            <div class="form-group">
                <label>Kota</label>
                <input name="kota" id="kota" type="text" class="form-control" value="<?php echo Pengaturan::kota(); ?>" maxlength="32" required />
            </div>

            <div class="form-group">
                <label>Aktifkan Hutang</label>
                <select name="hutang" id="hutang" class="form-control select2" style="width:100%" required>
                    <option value="y" <?php echo (Pengaturan::hutang() == "y") ? "selected" : ""; ?>>Iya</option>
                    <option value="t" <?php echo (Pengaturan::hutang() == "t") ? "selected" : ""; ?>>Tidak</option>
                </select>
            </div>

            <div class="form-group">
                <label>Aktifkan Piutang</label>
                <select name="piutang" id="piutang" class="form-control select2" style="width:100%" required>
                    <option value="y" <?php echo (Pengaturan::piutang() == "y") ? "selected" : ""; ?>>Iya</option>
                    <option value="t" <?php echo (Pengaturan::piutang() == "t") ? "selected" : ""; ?>>Tidak</option>
                </select>
            </div>

            <div class="form-group">
                <label>Kode Lisensi</label>
                <input name="license" id="license" type="text" class="form-control" value="<?php echo Pengaturan::license(); ?>" maxlength="29" required />
            </div-->
        </div>
        <!--div class="col-md-4">
            <div class="form-group">
                <label>Kode Pos</label>
                <input name="kodepos" id="kodepos" type="number" class="form-control" value="<?php echo Pengaturan::kodePos(); ?>" maxlength="5" required />
            </div>

            <div class="form-group">
                <label>Provinsi</label>
                <input name="provinsi" id="provinsi" type="text" class="form-control" value="<?php echo Pengaturan::provinsi(); ?>" maxlength="32" required />
            </div>

            <div class="form-group">
                <label>No. Telp</label>
                <input name="telp" id="telp" type="text" class="form-control" value="<?php echo Pengaturan::telp(); ?>" maxlength="15" required />
            </div>

            <div class="form-group">
                <label>Ucapan Terimakasih</label>
                <input name="ucapan" id="ucapan" type="text" class="form-control" value="<?php echo Pengaturan::ucapan(); ?>" maxlength="32" required />
            </div>

            <div class="form-group">
                <label>Aktifkan Barcode</label>
                <select name="barcode" id="barcode" class="form-control select2" style="width:100%" required>
                    <option value="y" <?php echo (Pengaturan::barcode() == "y") ? "selected" : ""; ?>>Iya</option>
                    <option value="t" <?php echo (Pengaturan::barcode() == "t") ? "selected" : ""; ?>>Tidak</option>
                </select>
            </div>

            <div class="form-group">
                <label>Aktifkan Stock</label>
                <select name="stock" id="stock" class="form-control select2" style="width:100%" required>
                    <option value="y" <?php echo (Pengaturan::stock() == "y") ? "selected" : ""; ?>>Iya</option>
                    <option value="t" <?php echo (Pengaturan::stock() == "t") ? "selected" : ""; ?>>Tidak</option>
                </select>
            </div>

            <div class="form-group">
                <label>Email Aktivasi</label>
                <input name="email" id="email" type="text" class="form-control" value="<?php echo Pengaturan::email(); ?>" maxlength="32" required />
            </div>
        </div-->
        <!--div class="col-md-4">
            <div class="form-group">
                <label>Path Printer Windows</label>
                <input name="path_windows" id="path_windows" type="text" class="form-control" value="<?php echo Pengaturan::pathWindows(); ?>" maxlength="25" required />
            </div>

            <div class="form-group">
                <label>Path Printer Linux</label>
                <input name="path_linux" id="path_linux" type="text" class="form-control" value="<?php echo Pengaturan::pathlinux(); ?>" maxlength="32" required />
            </div>

            <div class="form-group">
                <label>Cetak Struk Pembelian</label>
                <select name="cetak_pembelian" id="cetak_pembelian" class="form-control select2" style="width:100%" required>
                    <option value="y" <?php echo (Pengaturan::cetakPembelian() == "y") ? "selected" : ""; ?>>Iya</option>
                    <option value="t" <?php echo (Pengaturan::cetakPembelian() == "t") ? "selected" : ""; ?>>Tidak</option>
                </select>
            </div>

            <div class="form-group">
                <label>Cetak Struk Penjualan</label>
                <select name="cetak_penjualan" id="cetak_penjualan" class="form-control select2" style="width:100%" required>
                    <option value="y" <?php echo (Pengaturan::cetakPenjualan() == "y") ? "selected" : ""; ?>>Iya</option>
                    <option value="t" <?php echo (Pengaturan::cetakPenjualan() == "t") ? "selected" : ""; ?>>Tidak</option>
                </select>
            </div>

            <div class="form-group">
                <label>Desain Minimalis</label>
                <select name="desain" id="desain" class="form-control select2" style="width:100%" required>
                    <option value="y" <?php echo (Pengaturan::desain() == "y") ? "selected" : ""; ?>>Iya</option>
                    <option value="t" <?php echo (Pengaturan::desain() == "t") ? "selected" : ""; ?>>Tidak</option>
                </select>
            </div>

            <div class="form-group">
                <label>Aktifkan Multi Market</label>
                <select name="multimarket" id="multimarket" class="form-control select2" style="width:100%" required>
                    <option value="y" <?php echo (Pengaturan::multimarket() == "y") ? "selected" : ""; ?>>Iya</option>
                    <option value="t" <?php echo (Pengaturan::multimarket() == "t") ? "selected" : ""; ?>>Tidak</option>
                </select>
            </div>

            <div class="form-group">
                <label>IP / Domain Server</label>
                <input name="host" id="host" type="text" class="form-control" value="<?php echo Pengaturan::host(); ?>" maxlength="32" required />
            </div>
        </div-->

        <div class="clearfix"></div>
        <div class="col-md-3">
            <div class="form-group">
                <button class="btn btn-primary" name="simpan"><i class="fa fa-save"></i> &nbsp; Simpan</button>
            </div>
        </div>
    </form>
</div>

