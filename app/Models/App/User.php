<?php namespace App;

use BITS\BITS;
use BITS\Upload;
use Tools\Alert;

/**
 * PHP version 5
 *
 * User CRUD Services.
 * Class to simply use CRUD Data Management support PDO and PHP OOP.
 * This script written with Object Oriented Style (PSR-2) and use static method.
 *
 * @category Model
 * @package  BITS\BITS
 * @author   Nurul Imam <me@nurulimam.com>
 * @license  https://creativecommons.org/licenses/by-nc-nd/3.0/ Creative Commons
 * @link     https://bits.co.id
 */
class User
{
    /**
     * Get All Users data.
     *
     * @return array Data all users.
     */
    public static function all()
    {
        return BITS::all("users");
    }

    /**
     * Add new user.
     *
     * @return void
     */
    public static function add()
    {
        $_POST['password'] = password_hash($_POST['password'], PASSWORD_DEFAULT);
        $_POST['photo'] = Upload::upload($_FILES['photo']['name'], $_FILES['photo']['tmp_name'], "photo/");

        BITS::add('users', ['name', 'nip', 'jabatan', 'username', 'email', 'hp', 'password', 'level', 'photo']);
        Alert::add("success", "Successfully created !");
    }

    /**
     * Find User by ID
     *
     * @param int $ids ID of user.
     *
     * @return array    Data user.
     */
    public static function find($ids)
    {
        return BITS::find("users", "id", $ids);
    }

    /**
     * Update User by ID
     *
     * @return action Redirect to List all users.
     */
    public static function update()
    {
        /**
         * Change User Password if fill the password field.
         */
        if (!empty($_POST['password'])) {
            $_POST['password'] = password_hash($_POST['password'], PASSWORD_DEFAULT);
            BITS::update("users", ['password'], "id", $_POST['id']);
            Alert::add("success", "Password updated !");
        }

        if (!empty($_FILES['photo']['tmp_name'])) {
            $_POST['photo'] = Upload::upload($_FILES['photo']['name'], $_FILES['photo']['tmp_name'], "photo/");
            BITS::update("users", ['photo'], "id", $_POST['id']);
        }

        /**
         * Update User Data Without Photo & Password.
         */
        BITS::update('users', ['name', 'nip', 'jabatan', 'username', 'email', 'hp', 'level'], "id", $_POST['id']);
        Alert::add("success", "Successfully updated !");
    }

    /**
     * Delete User by ID.
     *
     * @param int $ids ID user.
     *
     * @return action   Redirect to List all users.
     */
    public static function delete($ids)
    {
        Alert::add("success", "Successfully removed !");
        return BITS::delete("users", "id", $ids);
    }

    /**
     * Get name of user to display.
     *
     * @return string Name User
     */
    public static function getName()
    {
        $data = BITS::find("users", "username", isset($_SESSION['username']) ? $_SESSION['username'] : "");
        echo isset($data[0]['name']) ? $data[0]['name'] : "";
    }

    /**
     * Get photo link of user to display.
     *
     * @return string Photo link of user
     */
    public static function getPhoto()
    {
        $data = BITS::find("users", "username", isset($_SESSION['username']) ? $_SESSION['username'] : "");
        echo isset($data[0]['photo']) ? $data[0]['photo'] : "";
    }
}
