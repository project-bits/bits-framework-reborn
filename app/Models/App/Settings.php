<?php namespace App;

use BITS\BITS;
use Tools\Alert;

/**
 * PHP version 5
 *
 * Settings CRUD Services.
 * Class to simply use CRUD Data Management support PDO and PHP OOP.
 * This script written with Object Oriented Style (PSR-2) and use static method.
 *
 * @category Model
 * @package  BITS\BITS
 * @author   Nurul Imam <me@nurulimam.com>
 * @license  https://creativecommons.org/licenses/by-nc-nd/3.0/ Creative Commons
 * @link     https://bits.co.id
 */
class Settings
{
    /**
     * Get value of name.
     *
     * @return string Name of Application
     */
    public static function name()
    {
        $data = BITS::custom("SELECT value FROM settings WHERE param = 'name'");
        return $data[0]['value'];
    }

    /**
     * Get value of address.
     *
     * @return string Address of Application
     */
    public static function address()
    {
        $data = BITS::custom("SELECT value FROM settings WHERE param = 'address'");
        return $data[0]['value'];
    }

    /**
     * Update Settings by ID
     *
     * @return action Redirect to List all settings.
     */
    public static function update()
    {
        $_POST['value'] = $_POST['name'];
        BITS::update('settings', ['value'], "param", "'name'");
        Alert::add("success", "Successfully updated !");
    }
}
