<?php namespace App;

use BITS\BITS;
use BITS\SQL;

/**
 * PHP version 5
 *
 * Backup CRUD Services.
 * Class to simply use CRUD Data Management support \PDO and PHP OOP.
 * This script written with Object Oriented Style (PSR-2) and use static method.
 *
 * @category Model
 * @package  BITS\SQL
 * @author   Nurul Imam <me@nurulimam.com>
 * @license  https://creativecommons.org/licenses/by-nc-nd/3.0/ Creative Commons
 * @link     https://bits.co.id
 */
class Backup extends SQL
{
    /**
     * Backup All Database
     *
     * @param string $tables Name Table of Database to Backup
     *
     * @return void
     */
    public static function now($tables = '*')
    {
        $data = "";
        if ($tables == '*') {
            $tables = array();

            $result = parent::$connection->prepare('SHOW TABLES');
            $result->execute();
            while ($row = $result->fetch(\PDO::FETCH_NUM)) {
                $tables[] = $row[0];
            }
        } else {
            $tables = is_array($tables) ? $tables : explode(',', $tables);
        }

        foreach ($tables as $table) {
            $resultcount = parent::$connection->prepare('SELECT count(*) FROM '.$table);
            $resultcount->execute();
            $num_fields = $resultcount->fetch(\PDO::FETCH_NUM);
            $num_fields = $num_fields[0];

            $result = parent::$connection->prepare('SELECT * FROM '.$table);
            $result->execute();
            $data.= 'DROP TABLE '.$table.';';

            $result2 = parent::$connection->prepare('SHOW CREATE TABLE '.$table);
            $result2->execute();
            $row2 = $result2->fetch(\PDO::FETCH_NUM);
            $data.= "\n\n".$row2[1].";\n\n";

            for ($i = 0; $i < $num_fields; $i++) {
                while ($row = $result->fetch(\PDO::FETCH_NUM)) {
                    $data.= 'INSERT INTO '.$table.' VALUES(';
                    for ($j=0; $j<$num_fields; $j++) {
                        $row[$j] = addslashes($row[$j]);
                        $row[$j] = str_replace("\n", "\\n", $row[$j]);
                        if (isset($row[$j])) {
                            $data.= '"'.$row[$j].'"' ;
                        } else {
                            $data.= '""';
                        }
                        if ($j<($num_fields-1)) {
                            $data.= ',';
                        }
                    }
                    $data.= ");\n";
                }
            }
            $data.="\n\n\n";
        }
        mkdir('backup', 755);
        $handle = fopen('backup/db-'.date('Y-m-d').'.txt', 'w+');
        fwrite($handle, $data);
        fclose($handle);
        return 'db-'.date('Y-m-d').'.txt';
    }
}
