<?php
/**
 * PHP version 5
 * Class & Function Library
 * Library all functions use rendering frontend template.
 * Call directly in controller to use function and use object defined in controller.
 *
 * Do not assign parameter in function because not able call object with parameter.
 *
 * @category Model
 * @package  BITS\BITS
 * @author   Nurul Imam <me@nurulimam.com>
 * @license  https://creativecommons.org/licenses/by-nc-nd/3.0/ Creative Commons
 * @link     https://bits.co.id
 */
use BITS\BITS;
use App\Settings;

/**
 * Basic Web Apps Configurations
 */
if (Settings::name()) {
    define('APPNAME', Settings::name());
} else {
    define('APPNAME', 'BITS Frameworks');
}

/**
 * SEO Library
 */

/**
 * Fetch Web Description From Database and return APPDESC is not exist.
 *
 * @return string Web Description.
 */
function weburl()
{
    echo"/";
}

/**
 * Get Dynamic Title.
 *
 * @return string Title of Post.
 */
function webtitle()
{
    if (is_null(APPTITLE)) {
        echo APPNAME." - ".APPDESC;
    } else {
        echo APPTITLE." - ".APPNAME;
    }
}
