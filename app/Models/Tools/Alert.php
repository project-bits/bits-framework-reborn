<?php namespace Tools;

/**
 * PHP version 5
 *
 * BITS Alert Services.
 * Class to simply use Alert Sevices support PDO and PHP OOP.
 * This script written with Object Oriented Style (PSR-2) and use static method.
 *
 * @category Model
 * @package  BITS\BITS
 * @author   Nurul Imam <me@nurulimam.com>
 * @license  https://creativecommons.org/licenses/by-nc-nd/3.0/ Creative Commons
 * @link     https://bits.co.id
 */
class Alert
{
    /**
     * Show alert messages
     *
     * @return string
     */
    public static function show()
    {
        if (isset($_SESSION['message'])) {
            echo '
            <div class="alert alert-'.$_SESSION['alert'].' fade in">
                '.$_SESSION['message'].'
            </div>';
            self::delete();
        }
    }

    /**
     * Display alert messages
     *
     * @return string
     */
    public static function view()
    {
        if (isset($_SESSION['message'])) {
            echo "
                    setTimeout(function() {
                        toastr.options = {
                            closeButton: true,
                            progressBar: true,
                            positionClass: 'toast-top-right',
                            showDuration: 400,
                            hideDuration: 1000,
                            extendedTimeOut: 1000,
                            showEasing: 'swing',
                            hideEasing: 'linear',
                            showMethod: 'fadeIn',
                            hideMethod: 'fadeOut',
                            timeOut: 7000
                        };
                        toastr.".$_SESSION['alert']."("."'".$_SESSION['message']."');
                    }, 1300);
                ";
        }
        self::delete();
    }

    /**
     * Create alert messages
     *
     * @param string $type    Danger, Warning or Success
     * @param string $message Text of alerts
     *
     * @return void
     */
    public static function add($type, $message)
    {
        $_SESSION['alert']   = $type;
        $_SESSION['message'] = $message;
    }

    /**
     * Delete alert messages
     *
     * @return void
     */
    public static function delete()
    {
        unset($_SESSION['alert']);
        unset($_SESSION['message']);
    }
}
