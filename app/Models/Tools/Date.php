<?php namespace Tools;

/**
 * PHP version 5
 *
 * Date Manipulation / Convert Tools.
 *
 * Class to simply Allow to generate format from date().
 * This script written with Object Oriented Style (PSR-2) and use static method.
 *
 * @category Model
 * @package  BITS\BITS
 * @author   Nurul Imam <me@nurulimam.com>
 * @license  https://creativecommons.org/licenses/by-nc-nd/3.0/ Creative Commons
 * @link     https://bits.co.id
 */
class Date
{
    /**
     * Convert Date Format.
     *
     * @param date   $date Date generated from date().
     * @param string $type stripe, slash, time
     *
     * @return date  Date formatted.
     */
    public static function convert($date, $type = "stripe")
    {
        if ($type == "stripe") {
            $data = date('d - F - Y', strtotime($date));
        } elseif ($type == "slash") {
            $data = date('d/m/Y', strtotime($date));
        } elseif ($type == "back") {
            $data = date('y/m/d', strtotime($date));
        } elseif ($type == "time") {
            $data = date('H:i', strtotime($date));
        } elseif ($type == "timestamp") {
            $data = date('Y-F-d H:i:s', strtotime($date));
        } elseif ($type == "full") {
            $data = date('l, d F Y H:i:s', strtotime($date));
        } elseif ($type == "day") {
            $data = date('d', strtotime($date));
        } elseif ($type == "month") {
            $data = date('M', strtotime($date));
        } elseif ($type == "year") {
            $data = date('Y', strtotime($date));
        } else {
            $data = date('Y-m-d');
        }
        return $data;
    }
}
