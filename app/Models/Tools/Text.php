<?php namespace Tools;

/**
 * PHP version 5
 *
 * Text Manipulation / Convert Tools.
 *
 * Class to simply Allow to manipulate text / string.
 * This script written with Object Oriented Style (PSR-2) and use static method.
 *
 * @category Model
 * @package  BITS\BITS
 * @author   Nurul Imam <me@nurulimam.com>
 * @license  https://creativecommons.org/licenses/by-nc-nd/3.0/ Creative Commons
 * @link     https://bits.co.id
 */
class Text
{
    /**
     * Truncate a string provided by the maximum limit without breaking a word.
     *
     * @param string $str Text
     * @param string $max Number Maximum
     *
     * @return string
     */
    public static function excerpt($str, $max)
    {
        if (strlen($str) > $max) {
            return substr($str, 0, $max).'...';
        } else {
            return $str;
        }
    }

    /**
     * Truncate a string provided by the maximum limit without breaking a word.
     *
     * @param string $str
     * @param string $panjang
     *
     * @return string
     */
    public static function fill($str, $panjang)
    {
        if (strlen($str) > $panjang) {
            return substr($str, 0, $panjang);
        } else {
            return str_pad($str, $panjang);
        }
    }

    /**
     * Truncate a string provided by the maximum limit without breaking a word.
     *
     * @param string $str
     * @param string $panjang
     *
     * @return string
     */
    public static function fillRight($str, $panjang)
    {
        if (strlen($str) > $panjang) {
            return substr($str, 0, $panjang);
        } else {
            return str_pad($str, $panjang, " ", STR_PAD_LEFT);
        }
    }

    public static function addComma($data)
    {
        $join = '';
        foreach ($data as $isi) {
            $join .= $isi.',';
        }
        return rtrim($join, ',');
    }
}
