<?php namespace Tools;

/**
 * PHP version 5
 *
 * API Client.
 *
 * Class to simply use API Data Management support PDO and PHP OOP.
 * This script written with Object Oriented Style (PSR-2) and use static method.
 *
 * @category Model
 * @package  BITS\BITS
 * @author   Nurul Imam <me@nurulimam.com>
 * @license  https://creativecommons.org/licenses/by-nc-nd/3.0/ Creative Commons
 * @link     https://bits.co.id
 */
class API
{
    public static $url;
    public static $method;

    /**
     * Get Data JSON
     *
     * @param array $json Parameter
     *
     * @return array       JSON Data
     */
    public static function get($json = null)
    {
        header("Access-Control-Allow-Orgin: *");
        header("Access-Control-Allow-Methods: *");
        header("Content-Type: application/json");

        $curl = curl_init();
        curl_setopt_array(
            $curl,
            array(
                CURLOPT_URL            => self::$url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "",
                CURLOPT_MAXREDIRS      => 10,
                CURLOPT_TIMEOUT        => 30,
                CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST  => self::$method,
                CURLOPT_POSTFIELDS     => $json
            )
        );

        $response = curl_exec($curl);
        $error = curl_error($curl);
        curl_close($curl);
        return ($error) ? "cURL Error # : ".$error : $response;
    }

    /**
     * Check License of BITS License Management System
     *
     * @param string $email   Email Registered
     * @param string $host    Host of LMS Server
     * @param string $license Serial Number
     *
     * @return array
     */
    public static function check($email, $host, $license)
    {
        self::$url = "https://license.bits.co.id/validate/".$email."/".$host."/".$license;
        self::$method = "GET";

        return self::get();
    }

    /**
     * NusaSMS Integration
     *
     * @param string $message     Text of Message
     * @param int    $destination Number Destination
     *
     * @return array
     */
    public static function sendSMS($message, $destination)
    {
        $curl = curl_init();
        curl_setopt_array(
            $curl,
            array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => 'http://api.nusasms.com/api/v3/sendsms/plain',
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => array(
                    'user' => 'dafieinc_api',
                    'password' => 'admin123',
                    'SMSText' => $message,
                    'GSM' => $destination
                )
            )
        );
        $response = curl_exec($curl);
        $error = curl_error($curl);
        curl_close($curl);
        return ($error) ? "cURL Error # : ".$error : $response;
    }

    /**
     * Check Credit NusaSMS
     *
     * @return array
     */
    public static function checkCredit()
    {
        $curl = curl_init();
        curl_setopt_array(
            $curl,
            array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => 'http://api.nusasms.com/api/command',
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => array(
                    'user' => 'imam',
                    'password' => 'admin123',
                    'cmd' => 'CREDITS',
                    'output' => 'json'
                )
            )
        );
        $response = curl_exec($curl);
        $error = curl_error($curl);
        curl_close($curl);
        return ($error) ? "cURL Error # : ".$error : $response;
    }
}
