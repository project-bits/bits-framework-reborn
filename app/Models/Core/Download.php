<?php namespace BITS;

/**
 * PHP version 5
 *
 * BITS Download Services.
 * Class to simply upload file with additional name and make new folder if not exist.
 * This script written with Object Oriented Style (PSR-2) and use static method.
 *
 * @category Model
 * @package  BITS\BITS
 * @author   Nurul Imam <me@nurulimam.com>
 * @license  https://creativecommons.org/licenses/by-nc-nd/3.0/ Creative Commons
 * @link     https://bits.co.id
 */
class Download
{
    public static $download_hook = array();

    private static $_args = array(
                        'download_path'         =>  null,
                        'file'                  =>  null,
                        'extension_check'       =>  true,
                        'referrer_check'        =>  false,
                        'referrer'              =>  null,
                    );

    private static $_allowed_extensions = array(
                        /* Archives */
                        'zip'   => 'application/zip',
                        '7z'    => 'application/octet-stream',
                        /* Documents */
                        'txt'   => 'text/plain',
                        'pdf'   => 'application/pdf',
                        'doc'   => 'application/msword',
                        'xls'   => 'application/vnd.ms-excel',
                        'ppt'   => 'application/vnd.ms-powerpoint',
                        /* Executables */
                        'exe'   => 'application/octet-stream',
                        /* Images */
                        'gif'   => 'image/gif',
                        'png'   => 'image/png',
                        'jpg'   => 'image/jpeg',
                        'jpeg'  => 'image/jpeg',
                        /* Audio */
                        'mp3'   => 'audio/mpeg',
                        'wav'   => 'audio/x-wav',
                        /* Video */
                        'mpeg'  => 'video/mpeg',
                        'mpg'   => 'video/mpeg',
                        'mpe'   => 'video/mpeg',
                        'mov'   => 'video/quicktime',
                        'avi'   => 'video/x-msvideo'
                    );


    /**
     * Constructor
     *
     * @param array $_args               Args
     * @param array $_allowed_extensions Extension
     *
     * @return void
     */
    public function __construct($_args = array(), $_allowed_extensions = array())
    {
        self::_setArgs($_args);
        self::_setAllowedExt($_allowed_extensions);
    }

    /**
     * Chip Print
     *
     * @param string $var Var
     *
     * @return void
     */
    public static function chipPrint($var)
    {
        echo "<pre>";
        print_r($var);
        echo "</pre>";
    }

    /**
     * Update default arguments
     * It will update default array of class i.e $_args
     *
     * @param array $_args    Input Arguments
     * @param array $defaults Default Arguments
     *
     * @return array
     */
    private static function _chipParseArgs($_args = array(), $defaults = array())
    {
        return array_merge($defaults, $_args);
    }

    /**
     * Get extension and name of file
     *
     * @param string $file_name Filename
     *
     * @return array
     */
    private static function _chipExtension($file_name)
    {
        $temp = array();
        $temp['file_name'] = strtolower(substr($file_name, 0, strripos($file_name, '.')));
        $temp['file_extension'] = strtolower(substr($file_name, strripos($file_name, '.') + 1));
        return $temp;
    }

    /**
     * Set default arguments
     * It will set default array od class i.e $_args
     *
     * @param array $_args Arguments
     *
     * @return void
     */
    private static function _setArgs($_args = array())
    {
        $defaults = self::getArgs();
        $_args = self::_chipParseArgs($_args, $defaults);
        self::$_args = $_args;
    }

    /**
     * Get default arguments
     * It will get default array of class i.e $_args
     *
     * @return array
     */
    public static function getArgs()
    {
        return self::$_args;
    }

    /**
     * Set default allowed extensions
     * It will set default array of class i.e $_allowed_extensions
     *
     * @param array $_allowed_extensions Allowed Extensions
     *
     * @return void
     */
    private static function _setAllowedExt($_allowed_extensions = array())
    {
        $defaults = self::getAllowedExt();
        $_allowed_extensions = array_unique(self::_chipParseArgs($_allowed_extensions, $defaults));
        self::$_allowed_extensions = $_allowed_extensions;
    }

    /**
     * Get default allowed extensions
     * It will get default array of class i.e $_allowed_extensions
     *
     * @return array
     */
    public static function getAllowedExt()
    {
        return self::$_allowed_extensions;
    }

    /**
     * Set Mime Type
     * It will set default array of class i.e $_allowed_extensions
     *
     * @param string $file_path Path of file
     *
     * @return string
     */
    private static function _setMimeType($file_path)
    {
        /* by Function - mime_content_type */
        if (function_exists('mime_content_type')) {
            $file_mime_type = @mime_content_type($file_path);
        } elseif (function_exists('finfo_file')) {
            $finfo = @finfo_open(FILEINFO_MIME);
            $file_mime_type = @finfo_file($finfo, $file_path);
            finfo_close($finfo);
        } else {
            $file_mime_type = false;
        }

        return $file_mime_type;
    }

    /**
     * Get Mime Type
     * It will set default array of class i.e $_allowed_extensions
     *
     * @param string $file_path Path of File
     *
     * @return string
     */
    public static function getMimeType($file_path)
    {
        return self::_setMimeType($file_path);
    }

    /**
     * Pre Download Hook
     *
     * @return void
     */
    private static function _setDownloadHook()
    {
        /* Allowed Extensions */
        $_allowed_extensions = self::getAllowedExt();

        /* Arguments */
        $_args = self::getArgs();

        /* Extract Arguments */
        extract($_args);

        /* Directory Depth */
        $dir_depth = dirname($file);
        if (!empty($dir_depth) && $dir_depth != ".") {
            $download_path = $download_path . $dir_depth . "/";
        }

        /* File Name */
        $file = basename($file);

        /* File Path */
        $file_path = $download_path . $file;
        self::$download_hook['file_path'] = $file_path;

        /* File and File Path Validation */
        if (empty($file) || !file_exists($file_path)) {
            self::$download_hook['download'] = false;
            self::$download_hook['message'] = "Invalid File or File Path.";
            return 0;
        }

        /* File Name and Extension */
        $nameext = self::_chipExtension($file);
        $file_name = $nameext['file_name'];
        $file_extension = $nameext['file_extension'];

        self::$download_hook['file'] = $file;
        self::$download_hook['file_name'] = $file_name;
        self::$download_hook['file_extension'] = $file_extension;

        /* Allowed Extension - Validation */
        if ($extension_check == true && !array_key_exists($file_extension, $_allowed_extensions)) {
            self::$download_hook['download'] = false;
            self::$download_hook['message'] = "File is not allowed to download";
            return 0;
        }

        /* Referrer - Validation */
        if ($referrer_check == true && !empty($referrer) && strpos(strtoupper($_SERVER['HTTP_REFERER']), strtoupper($referrer)) === false) {
            self::$download_hook['download'] = false;
            self::$download_hook['message'] = "Internal server error - Please contact system administrator";
            return 0;
        }

        /* File Size in Bytes */
        $file_size = filesize($file_path);
        self::$download_hook['file_size'] = $file_size;

        /* File Mime Type - Auto, Manual, Default */
        $file_mime_type = self::getMimeType($file_path);
        if (empty($file_mime_type)) {
            $file_mime_type = $_allowed_extensions[$file_extension];
            if (empty($file_mime_type)) {
                $file_mime_type = "application/force-download";
            }
        }

        self::$download_hook['file_mime_type'] = $file_mime_type;
        self::$download_hook['download'] = true;
        self::$download_hook['message'] = "File is ready to download";
        return 0;
    }

    /**
     * Download Hook
     * Allows you to do some action before download
     *
     * @return array
     */
    public static function getDownloadHook()
    {
        self::_setDownloadHook();
        return self::$download_hook;
    }

    /**
     * Post Download Hook
     *
     * @return array
     */
    private static function _setPostDownloadHook()
    {
        return self::$download_hook;
    }

    /**
     * Download
     * Start download stream
     *
     * @return void
     */
    public static function setDownload()
    {
        /* Download Hook */
        $download_hook = self::_setPostDownloadHook();

        /* Extract */
        extract($download_hook);

        /* Recheck */
        if ($download_hook['download'] != true) {
            echo "File is not allowed to download";
            return 0;
        }

        /* Execution Time Unlimited */
        set_time_limit(0);

        /**
        * Header
        * Forcing a download using readfile()
        **/

        header('Content-Description: File Transfer');
        header('Content-Type: ' . $file_mime_type);
        header('Content-Disposition: attachment; filename=' . $file);
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . $file_size);
        ob_clean();
        flush();
        readfile($file_path);
        exit;
    }

    /**
     * Download
     * Start download stream
     *
     * @return array
     */
    public static function getDownload()
    {
        self::setDownload();
        exit;
    }
}
