<?php namespace BITS;

/**
 * PHP version 5
 *
 * BITS Upload Services.
 * Class to simply upload file with additional name and make new folder if not exist.
 * This script written with Object Oriented Style (PSR-2) and use static method.
 *
 * @category Model
 * @package  BITS\BITS
 * @author   Nurul Imam <me@nurulimam.com>
 * @license  https://creativecommons.org/licenses/by-nc-nd/3.0/ Creative Commons
 * @link     https://bits.co.id
 */
class Upload
{
    /**
     * Upload a file to server
     *
     * @param string $file   Name of file
     * @param string $temp   Temp of file
     * @param string $folder Name of folder
     *
     * @return string         Name of Path File Uploaded
     */
    public static function upload($file, $temp, $folder)
    {
        if ($file != '') {
            $name  = strtolower(str_replace(' ', '-', $file));
            $data  = $folder.$name;

            if (is_dir($folder) == false) {
                mkdir("$folder", 0755);
            }

            if (is_dir($data) == false) {
                move_uploaded_file($temp, $data);
            }

            return $name;
        } else {
            return '';
        }
    }

    /**
     * Upload a file into summernote editor.
     *
     * @return string Path of file uploaded
     */
    public static function summernote()
    {
        if ($_FILES['file']['name']) {
            if (!$_FILES['file']['error']) {
                $file = 'files/img/'.date('Y-m-d').'-'.strtolower(str_replace(' ', '-', $_FILES['file']['name']));
                $temp = $_FILES["file"]["tmp_name"];
                if (is_dir($file) == false) {
                    move_uploaded_file($temp, $file);
                }
                echo "/".$file;
            } else {
                echo 'Ooops!  Your upload triggered the following error : '.$_FILES['file']['error'];
            }
        }
    }
}
