<?php namespace BITS;

/**
 * PHP version 5
 *
 * BITS Auth Services.
 * Class to simply use Auth Sevices support PDO and PHP OOP.
 * This script written with Object Oriented Style (PSR-2) and use static method.
 *
 * @category Model
 * @package  BITS\BITS
 * @author   Nurul Imam <me@nurulimam.com>
 * @license  https://creativecommons.org/licenses/by-nc-nd/3.0/ Creative Commons
 * @link     https://bits.co.id
 */
class Auth extends BITS
{

    /**
     * Check Username to database. If exist, return password validation.
     *
     * @param string $table    Table User.
     * @param string $username Input username data.
     * @param string $password Input password data.
     * @param string $fuser    Field username of table.
     * @param string $fpass    Field password of table.
     *
     * @return string           Session of username and salt.
     */
    public static function login($table, $username, $password, $fuser = "username", $fpass = "password")
    {
        $userdata = parent::find($table, $fuser, $username);
        if (empty($userdata)) {
            $_SESSION['alert'] = 'danger';
            $_SESSION['message'] = 'Username is not registered...!';
        } else {
            if (password_verify($password, $userdata[0][$fpass])) {
                self::generateSalt();
                $_SESSION['id']       = $userdata[0]['id'];
                $_SESSION['username'] = $userdata[0][$fuser];
                $_SESSION['name']     = $userdata[0]['name'];
                $_SESSION['level']    = $userdata[0]['level'];
            } else {
                $_SESSION['alert'] = 'danger';
                $_SESSION['message'] = 'Wrong Password...!';
            }
        }
    }

    /**
     * Generate Salt Fingerprint.
     *
     * @return string salt.
     */
    public static function generateSalt()
    {
        $fingerprint = md5($_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT'].date('d.m.Y'));
        if (isset($_SESSION['_USER_LOOSE_IP']) != long2ip(ip2long($_SERVER['REMOTE_ADDR'])  & ip2long("255.255.0.0"))
            || isset($_SESSION['_REMOTE_ADDR']) !== $_SERVER['REMOTE_ADDR']
            || isset($_SESSION['_USER_AGENT']) != $_SERVER['HTTP_USER_AGENT']
            || isset($_SESSION['_USER_ACCEPT']) != $_SERVER['HTTP_ACCEPT']
            || isset($_SESSION['_USER_ACCEPT_ENCODING']) != $_SERVER['HTTP_ACCEPT_ENCODING']
            || isset($_SESSION['_USER_ACCEPT_LANG']) != $_SERVER['HTTP_ACCEPT_LANGUAGE']
            || isset($_SESSION['salt']) !== $fingerprint) {
            session_unset();
            session_destroy();
            setcookie("sid", session_id(), strtotime("+1 hour"), "/", ".bits.co.id", true, true);
            session_start();
            session_regenerate_id(true);
            $_SESSION['_REMOTE_ADDR']          = $_SERVER['REMOTE_ADDR'];
            $_SESSION['_USER_AGENT']           = $_SERVER['HTTP_USER_AGENT'];
            $_SESSION['_USER_ACCEPT']          = $_SERVER['HTTP_ACCEPT'];
            $_SESSION['_USER_ACCEPT_ENCODING'] = $_SERVER['HTTP_ACCEPT_ENCODING'];
            $_SESSION['_USER_ACCEPT_LANG']     = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
            $_SESSION['_USER_LOOSE_IP']        = long2ip(ip2long($_SERVER['REMOTE_ADDR']) & ip2long("255.255.0.0"));
            $_SESSION['salt']                  = $fingerprint;
        }
    }

    /**
     * Redirecting to a url.
     *
     * @param string $url Url to redirect a page.
     *
     * @return object Redirecting to page.
     */
    public static function redirect($url)
    {
        header("Location: $url");
        exit();
    }

    /**
     * Check Level User Admin
     *
     * @return bool True/False
     */
    public static function checkAdmin()
    {
        if (isset($_SESSION['level'])) {
            if ($_SESSION['level'] == "admin") {
                return true;
            }
        }
    }

    /**
     * Check Level User Valid Login
     *
     * @return bool True/False
     */
    public static function validUsers()
    {
        if (isset($_SESSION['level'])) {
            return true;
        }
    }

    /**
     * User logout to destroy all session login.
     *
     * @return boolean Set to true if successfully logged out.
     */
    public static function logout()
    {
        session_destroy();
        return true;
    }
}
