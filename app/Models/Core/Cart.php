<?php namespace BITS;

/**
 * PHP version 5
 *
 * BITS Shopping Cart.
 * Class to simply use Shopping Cart support PDO and PHP OOP.
 * This script written with Object Oriented Style (PSR-2) and use static method.
 *
 * @category Model
 * @package  BITS\BITS
 * @author   Nurul Imam <me@nurulimam.com>
 * @license  https://creativecommons.org/licenses/by-nc-nd/3.0/ Creative Commons
 * @link     https://bits.co.id
 */
class Cart
{
    /**
     * Cart Checkout System
     *
     * @param string $sesname Name of Session
     * @param int    $code    ID / Barcode of Product
     * @param int    $qty     Quantity of Product
     * @param string $name    Name of Product
     * @param int    $price   Price of Product
     *
     * @return void
     */
    public static function add($sesname, $code, $qty, $name, $price)
    {
        if (!isset($_SESSION[$sesname][$code]['qty'])) {
            $_SESSION[$sesname][$code]['name']  = $name;
            $_SESSION[$sesname][$code]['price'] = $price;
            $_SESSION[$sesname][$code]['qty']   = $qty;
        } else {
            $_SESSION[$sesname][$code]['qty'] += $qty;
        }
    }

    /**
     * Decrease Product in Cart
     *
     * @param string $sesname Name of Session
     * @param int    $code    ID / Barcode of Product
     * @param int    $qty     Quantity of Product
     *
     * @return void
     */
    public static function min($sesname, $code, $qty)
    {
        if ($_SESSION[$sesname][$code]['qty'] == 1) {
            unset($_SESSION[$sesname][$code]);
        } else {
            $_SESSION[$sesname][$code]['qty'] -= $qty;
        }
    }

    /**
     * Delete Product in Cart
     *
     * @param string $sesname Name of Session
     * @param int    $code    ID / Barcode of Product
     *
     * @return void
     */
    public static function delete($sesname, $code)
    {
        unset($_SESSION[$sesname][$code]);
    }

    /**
     * Reset Cart
     *
     * @param string $sesname Name of Session
     *
     * @return void
     */
    public static function reset($sesname)
    {
        unset($_SESSION[$sesname]);
    }
}
